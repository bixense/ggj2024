#include "Highscore.hpp"

#include "Menu.hpp"
#include "ScoreKeeper.hpp"
#include "engine/Fade.hpp"

Highscore::Highscore() {
    static jngl::Font bigFont("fonts/BarcadeBrawlRegular-cc0.ttf", 15);
    container.addWidget<jngl::Label>("Fartastic, you won!", bigFont, 0xffffff_rgb,
                                     jngl::Vec2{ 0, -100 });
    static jngl::Font font("fonts/BarcadeBrawlRegular-cc0.ttf", 5);
    container.addWidget<jngl::Label>("Total number of deaths:", font, 0xeeeeee_rgb,
                                     jngl::Vec2{ -70, -50 });
    container.addWidget<jngl::Label>(std::to_string(ScoreKeeper::handle().getTotalDeaths()), font,
                                     0xeeeeee_rgb, jngl::Vec2{ 120, -50 });
    container.addWidget<jngl::Label>("FartyBird:", font, 0xdddddd_rgb,
                                     jngl::Vec2{ -70, 0 });
    container.addWidget<jngl::Label>(std::to_string(ScoreKeeper::handle().getDeathsFartyBird()),
                                     font, 0xdddddd_rgb, jngl::Vec2{ 120, 0 });
    container.addWidget<jngl::Label>("Ass Evaders:", font, 0xdddddd_rgb,
                                     jngl::Vec2{ -70, 30 });
    container.addWidget<jngl::Label>(std::to_string(ScoreKeeper::handle().getDeathsAssEvaders()),
                                     font, 0xdddddd_rgb, jngl::Vec2{ 120, 30 });
    container.addWidget<jngl::Label>("Whoopee Jump:", font, 0xdddddd_rgb,
                                     jngl::Vec2{ -70, 60 });
    container.addWidget<jngl::Label>(std::to_string(ScoreKeeper::handle().getDeathsWhoopeeJump()),
                                     font, 0xdddddd_rgb, jngl::Vec2{ 120, 60 });
    container.addWidget<jngl::Label>("Assteroids:", font, 0xdddddd_rgb,
                                     jngl::Vec2{ -70, 90 });
    container.addWidget<jngl::Label>(std::to_string(ScoreKeeper::handle().getDeathsAssteroids()),
                                     font, 0xdddddd_rgb, jngl::Vec2{ 120, 90 });
}

void Highscore::step() {
    container.step();
    control.step();
    if (control.fart()) {
        jngl::setWork<Fade>(std::make_shared<Menu>());
    }
}

void Highscore::draw() const {
    container.draw();
}
