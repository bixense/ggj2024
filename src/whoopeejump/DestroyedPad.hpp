#pragma once

#include "../GameObject.hpp"
#include "../engine/Animation.hpp"

#include <box2d/box2d.h>

class DestroyedPad : public GameObject {
public:
    explicit DestroyedPad(b2World&);
    Action step() override;
    void onContact(GameObject* other) override;

protected:
    bool normalpad;
    bool isdead;
private:
    void draw(jngl::Mat3) const override;

    Animation animation;
    const static jngl::Vec2 BB;
    b2Body& whoopee;
    b2PolygonShape shape;
};
