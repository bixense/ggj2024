#pragma once

#include "../GameObject.hpp"

#include <box2d/box2d.h>

class Game;

class JumpPad : public GameObject {
public:
    explicit JumpPad(Game&, bool top, b2Body&);
    Action step() override;
    void onContact(GameObject* other) override;

protected:
    bool normalpad;
    bool isdead;
private:
    void draw(jngl::Mat3) const override;

    jngl::Sprite sprite;
    const static jngl::Vec2 BB;
    b2Body& whoopee;
    b2PolygonShape shape;
	b2FixtureDef fixtureDef;
    b2Fixture* fixture = nullptr;
    Game& game;
    double jumpAnimation = 0;
};
