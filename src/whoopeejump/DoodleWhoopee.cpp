#include "DoodleWhoopee.hpp"
#include "JumpPad.hpp"

DoodleWhoopee::DoodleWhoopee(b2World& world) : Whoopee(world) {
    setPosition(jngl::Vec2(0, 170));
    body->ApplyForce(b2Vec2(0, -70), body->GetWorldCenter(), true);
    body->SetTransform(body->GetPosition(), M_PI / 2.);
}

auto DoodleWhoopee::step() -> Action {
    control->step();
    auto direction = control->getMovement();
    direction *= 2.5;
    float force = body->GetMass() * 900;
    body->ApplyForce(b2Vec2(direction.x, 0), body->GetWorldCenter(), true);
    if (grounded) {
        body->ApplyForce(b2Vec2(0, -force), body->GetWorldCenter(), true);
        animate();
    }
    grounded = false;
    return Whoopee::step();
}

void DoodleWhoopee::onContact(GameObject* other) {
    if (dynamic_cast<JumpPad*>(other)) {
        grounded = true;
    }
}
