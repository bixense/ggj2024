#include "WhoopeeJump.hpp"

#include "../assteroids/Assteroids.hpp"
#include "../constants.hpp"
#include "../cutscene/Cutscene1.hpp"
#include "../engine/Fade.hpp"
#include "../engine/MoveFade.hpp"
#include "DoodleWhoopee.hpp"
#include "JumpPad.hpp"

WhoopeeJump::WhoopeeJump(float musicVolume)
: Game(20, musicVolume), whoopee(new DoodleWhoopee(world)) {
    gameObjects.emplace_back(whoopee);
    const int padAmount = 40;
    for (int i = 0; i < padAmount; i++) {
        bool createBadPad = false;
        if (i != 0) {
            createBadPad = i > 8 && static_cast<int>(random() % 100) < badPadChance;
            if (random() % 2 == 0) {
                PadPosY -= (hightDistance - static_cast<int>(random() % hightDistanceOffset));
            } else {
                PadPosY -= (hightDistance + static_cast<int>(random() % hightDistanceOffset));
            }
            int offset = (static_cast<int>(random() % widthDistanceMax) + widthDistanceMin);
            if (random() % 2 == 0) {
                if (PadPosX + offset < widthBorder) {
                    PadPosX += offset;
                } else {
                    PadPosX -= offset;
                }
            } else {
                if (PadPosX - offset > -widthBorder) {
                    PadPosX -= offset;
                } else {
                    PadPosX += offset;
                }
            }
        }
        auto jumppad = new JumpPad(*this, !createBadPad, *whoopee->body);
        jumppad->setPosition(jngl::Vec2(PadPosX, PadPosY));
        gameObjects.emplace_back(jumppad);
        camera = jngl::Vec2(0, 0);
    }
}

void WhoopeeJump::step() {
    stepMusic();
    if (stepsLost >= 0) {
        ++stepsLost;
        if (stepsLost > 200) {
            jngl::setWork<Fade>(std::make_shared<WhoopeeJump>(volume));
        }
        return;
    }
    if (lost) {
        stepsLost = 0;
        return;
    }
    Game::step();
    if (highestPoint > whoopee->getPosition().y) {
        highestPoint = whoopee->getPosition().y;
    }
    if (whoopee->getPosition().y - highestPoint > failRange) {
        lost = true;
    }
    if (winCounter < 0 &&
        (highestPoint < -3000 || (jngl::keyDown(jngl::key::ControlL) && jngl::keyPressed('n')))) {
        winCounter = 0;
    }
    if (winCounter >= 0) {
        ++winCounter;
        whoopee->setPosition(whoopee->getPosition() - jngl::Vec2(0, 7));
    }
    if (winCounter > 120) {
        jngl::setWork<MoveFade>(std::make_shared<Cutscene1>(shared_from_this()));
    }
    if (whoopee->getPosition().y - camera.y < -cameraWaiter) {
        camera = whoopee->getPosition();
    }
    camera.x = 0;
    if (winCounter < 0) {
        actualCamera += 0.1 * (camera - actualCamera);
    }
    ++stepsPassed;
}

void WhoopeeJump::draw() const {
    constexpr bool DEBUG = false;
    if constexpr (DEBUG) {
        jngl::pushMatrix();
        jngl::scale(0.4);
    }
    jngl::pushMatrix();
    deathAnimation(whoopee->getPosition() - camera);
    {
        double bgScroll = -(std::lround(actualCamera.y / 2.4) % std::lround(bg.getHeight()));
        auto mv =
            jngl::modelview().translate({ 0, bgScroll + bg.getHeight() / 2. - SCREEN_HEIGHT / 2. });
        bg.draw(mv);
        bg.draw(jngl::Mat3(mv).translate({ 0, -bg.getHeight() }));
    }
    {
        double bgScroll = -(std::lround(actualCamera.y / 1.3) % std::lround(bg2.getHeight()));
        double xOffset = std::sin(stepsPassed / 900.) * 8;
        // xOffset = 0;
        auto mv =
            jngl::modelview().translate({ SCREEN_WIDTH / 2. - bg2.getWidth() / 2. + xOffset,
                                          bgScroll + bg2.getHeight() / 2. - SCREEN_HEIGHT / 2. });
        bg2.draw(mv);
        bg2.draw(jngl::Mat3(mv).translate({ 0, -bg2.getHeight() }));
        // bg2.draw(jngl::Mat3(mv).translate({ -bg2.getWidth(), -bg2.getHeight() }));
        // bg2.draw(jngl::Mat3(mv).translate({ bg2.getWidth(), -bg2.getHeight() }));
        // bg2.draw(jngl::Mat3(mv).translate({ -bg2.getWidth(), 0 }));
        // bg2.draw(jngl::Mat3(mv).translate({ bg2.getWidth(), 0 }));
    }
    auto cameraModelview = jngl::modelview().translate(-actualCamera);
    drawGameObjects(cameraModelview);
    {
        double bgScroll = -(std::lround(actualCamera.y * 1.4) % std::lround(bg3.getHeight()));
        double xOffset = std::cos(stepsPassed / 500.) * 11;
        // xOffset = 0;
        auto mv =
            jngl::modelview().translate({ SCREEN_WIDTH / 2. - bg3.getWidth() / 2. + xOffset,
                                          bgScroll + bg3.getHeight() / 2. - SCREEN_HEIGHT / 2. });
        jngl::setSpriteAlpha(180);
        bg3.draw(mv);
        bg3.draw(jngl::Mat3(mv).translate({ 0, -bg3.getHeight() }));
        // bg3.draw(jngl::Mat3(mv).translate({ -bg3.getWidth(), -bg3.getHeight() }));
        // bg3.draw(jngl::Mat3(mv).translate({ bg3.getWidth(), -bg3.getHeight() }));
        // bg3.draw(jngl::Mat3(mv).translate({ -bg3.getWidth(), 0 }));
        // bg3.draw(jngl::Mat3(mv).translate({ bg3.getWidth(), 0 }));
        jngl::setSpriteAlpha(255);
    }
    jngl::popMatrix();
    if(whoopee->control->hasTouched()){
        auto mvl = jngl::modelview().translate({-SCREEN_WIDTH / 2. +100, SCREEN_HEIGHT / 2. -50}).rotate(M_PI/2);
        auto mvr = jngl::modelview().translate({-SCREEN_WIDTH / 2. +50+SCREEN_WIDTH/4, SCREEN_HEIGHT / 2. -50}).rotate(M_PI*1.5);
        jngl::setSpriteAlpha(180);
        left.draw(mvl);
        right.draw(mvr);
        jngl::setSpriteAlpha(255);
    }
    if constexpr (DEBUG) {
        jngl::setColor(0xff0000_rgb, 255);
        jngl::drawLine(jngl::Vec2(-SCREEN_WIDTH / 2., -SCREEN_HEIGHT / 2.),
                       jngl::Vec2(-SCREEN_WIDTH / 2., SCREEN_HEIGHT / 2.));
        jngl::drawLine(jngl::Vec2(SCREEN_WIDTH / 2., -SCREEN_HEIGHT / 2.),
                       jngl::Vec2(SCREEN_WIDTH / 2., SCREEN_HEIGHT / 2.));
        jngl::drawLine(jngl::Vec2(-SCREEN_WIDTH / 2., SCREEN_HEIGHT / 2.),
                       jngl::Vec2(SCREEN_WIDTH / 2., SCREEN_HEIGHT / 2.));
        jngl::drawLine(jngl::Vec2(-SCREEN_WIDTH / 2., -SCREEN_HEIGHT / 2.),
                       jngl::Vec2(SCREEN_WIDTH / 2., -SCREEN_HEIGHT / 2.));
        jngl::popMatrix();
    }
}

std::shared_ptr<jngl::Work> WhoopeeJump::getNext() const {
    return std::make_shared<Assteroids>(volume);
}
