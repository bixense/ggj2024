#include "JumpPad.hpp"
#include "DoodleWhoopee.hpp"

#include "../constants.hpp"
#include "../Game.hpp"
#include "DestroyedPad.hpp"

const jngl::Vec2 JumpPad::BB(53, 20);

JumpPad::JumpPad(Game& game, bool padstate, b2Body& whoopee)
: sprite(padstate ? "whoopeejump/plattform_turdwithgrass" : "whoopeejump/plattform_smalltileable"),
whoopee(whoopee), game(game) {
    b2BodyDef bodyDef;
    bodyDef.position = pixelToMeter(jngl::Vec2(-100, 0));
    bodyDef.type = b2_staticBody;
    body = game.world.CreateBody(&bodyDef);
    body->SetGravityScale(0);
    body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
    body->SetLinearDamping(10.f);
    normalpad = padstate;
    isdead = false;
    if (normalpad) {
        shape.SetAsBox(pixelToMeter(BB.x / 2.), pixelToMeter(BB.y / 2.));
    } else {
        shape.SetAsBox(pixelToMeter(BB.x / 6.), pixelToMeter(BB.y / 6.));
    }
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = FILTER_CATEGORY_SOLID_OBJECT;
	fixtureDef.filter.maskBits = 0xffff;
	body->SetGravityScale(1);
}

auto JumpPad::step() -> Action {
    jumpAnimation *= 0.9;
    if (fixture) {
        if (whoopee.GetLinearVelocity().y < 0) {
            body->DestroyFixture(fixture);
            fixture = nullptr;
        }
    } else {
        if (whoopee.GetLinearVelocity().y > 0 && body->GetPosition().y > whoopee.GetPosition().y) {
            fixture = body->CreateFixture(&fixtureDef);
        }
    }
    if(isdead){
        auto world = &game.world;
        auto pos = getPosition();
        game.add([world, pos]() {
            auto p = std::make_shared<DestroyedPad>(*world);
            p->setPosition(pos);
            return p;
        });
        return Action::REMOVE_ME;
    }else{
    return Action::NONE;
    }
}

void JumpPad::draw(jngl::Mat3 mv) const {
    mv.translate(getPosition() + jngl::Vec2(0, jumpAnimation));
    sprite.draw(mv);
    jngl::setAlpha(200);
    //jngl::drawRect(mv.translate(BB / -2.), BB, 0xff0000_rgb);
}
void JumpPad::onContact(GameObject* other) {
	if (dynamic_cast<DoodleWhoopee*>(other))
	{
		if (normalpad) {
            jumpAnimation = -8;
        } else {
            isdead = true;
		}
	}
}
