#include "DestroyedPad.hpp"
#include "DoodleWhoopee.hpp"

#include "../constants.hpp"

const jngl::Vec2 DestroyedPad::BB(53, 20);

DestroyedPad::DestroyedPad(b2World& world)
: animation("whoopeejump/crumbleplattform"),
whoopee(whoopee) {
    b2BodyDef bodyDef;
    bodyDef.position = pixelToMeter(jngl::Vec2(-100, 0));
    bodyDef.type = b2_staticBody;
    body = world.CreateBody(&bodyDef);
    body->SetGravityScale(0);
    body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
    body->SetLinearDamping(10.f);
    shape.SetAsBox(pixelToMeter(BB.x / 2.), pixelToMeter(BB.y / 2.));
    body->SetGravityScale(1);
    animation.reset();
}
auto DestroyedPad::step() -> Action {
    animation.step();
    if(animation.done()){
        return Action::REMOVE_ME;
    }else{
    return Action::NONE;
    }
}

void DestroyedPad::draw(jngl::Mat3 mv) const {
    if (animation.done()) {
        return;
    }
    mv.translate(getPosition());
    animation.draw(mv);
    jngl::setAlpha(200);
    //jngl::drawRect(mv.translate(BB / -2.), BB, 0xff0000_rgb);
}
void DestroyedPad::onContact(GameObject* other) {
    assert(false);
}
