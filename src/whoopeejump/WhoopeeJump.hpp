#pragma once

#include "../Game.hpp"
#include "../constants.hpp"
#include <random>
#include "../Whoopee.hpp"


class DoodleWhoopee;

class WhoopeeJump : public Game, public std::enable_shared_from_this<WhoopeeJump> {
public:
    explicit WhoopeeJump(float musicVolume);
    void step() override;
    void draw() const override;

private:
    std::shared_ptr<jngl::Work> getNext() const override;

    int winCounter = -1; //< zählt hoch, sobald gewonnen
    std::random_device random;
    int hightDistance = 80;
    int hightDistanceOffset = 5;
    int widthDistanceMax = 100;
    int widthDistanceMin = 70;
    int widthBorder = SCREEN_WIDTH / 2 - 40 /* halbe Pad-Breite */;
    int PadPosX = 0;
    int PadPosY = 140;
    int badPadChance = 25;
    int cameraWaiter = 20;
    int highestPoint = 0;
    int failRange = 175;
    bool lost = false;
    jngl::Vec2 camera;
    jngl::Vec2 actualCamera;
    DoodleWhoopee* whoopee;
    jngl::Sprite bg{ "whoopeejump/background_horizon_layer" };
    jngl::Sprite bg2{ "whoopeejump/background_faraway_layer" };
    jngl::Sprite bg3{ "whoopeejump/background_near_layer" };
    jngl::Sprite left{ "fartibird/whoopee" };
    jngl::Sprite right{ "fartibird/whoopee" };
    int stepsPassed = 0;
};
