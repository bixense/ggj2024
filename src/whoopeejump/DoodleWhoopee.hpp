#pragma once

#include "../Whoopee.hpp"

class DoodleWhoopee : public Whoopee {
public:
    explicit DoodleWhoopee(b2World&);

    Action step() override;
    void onContact(GameObject* other) override;

protected:
    bool grounded = false;
};
