#pragma once

#include "Game.hpp"
#include "GameObject.hpp"

#include <box2d/box2d.h>

class Control;


class SpaceFart : public GameObject {
public:
    explicit SpaceFart(b2World&, jngl::Vec2 position, jngl::Vec2 force);
    ~SpaceFart() override;
    Action step() override;
    void setDeleted();
protected:
    jngl::Sprite sprite{"assteroids/firefart"};
    jngl::Vec2 force;
    void createFixtureFromShape(const b2Shape& shape);
private:
    void draw(jngl::Mat3) const override;

    constexpr static double RADIUS = 5.;
    int lifetime = 140;
    Action status = Action::NONE;
};
