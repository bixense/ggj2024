#include "Game.hpp"

#include "GameObject.hpp"
#include "Menu.hpp"
#include "ScoreKeeper.hpp"

#include <ranges>

Game::Game(float gravity, float musicVolume) : world(b2Vec2(0, gravity)), volume(musicVolume) {
    world.SetContactListener(&contactListener);
}

void Game::step() {
    removeObjects();
    world.Step(1.f / jngl::getStepsPerSecond(), 8, 3);

    for (auto it = gameObjects.begin(); it != gameObjects.end();) {
        switch ((*it)->step()) {
        case GameObject::Action::REMOVE_ME:
            it = gameObjects.erase(it);
            break;
        case GameObject::Action::NONE:
            ++it;
            break;
        }
    }
    addObjects();
}

void Game::addObjects() {
    for (const auto& factory : needToAdd) {
        gameObjects.emplace_back(factory());
    }
    needToAdd.clear();
}

void Game::removeObjects() {
    for (const auto& toRemove : needToRemove) {
        for (auto it = gameObjects.begin(); it != gameObjects.end(); ++it) {
            if (*it == toRemove) {
                gameObjects.erase(it);
                break;
            }
        }
    }
    needToRemove.clear();
}

void Game::drawGameObjects(const jngl::Mat3& modelview) const {
    for (const auto& gameObject : std::ranges::reverse_view(gameObjects)) {
        gameObject->draw(modelview);
    }
}

void Game::add(std::function<std::shared_ptr<GameObject>()> factory) {
    needToAdd.emplace_back(std::move(factory));
}

void Game::remove(const std::shared_ptr<GameObject>& object) {
    needToRemove.push_back(object);
}

std::shared_ptr<jngl::Work> Game::getNext() const {
    return std::make_shared<Menu>();
}

void Game::deathAnimation(jngl::Vec2 targetPosition) const {
    if (stepsLost >= 0) {
        jngl::scale(1 + (stepsLost / 100.));
        jngl::rotate(stepsLost / 10.);
        float t = stepsLost / 200.;
        if (t > 1) {
            t = 1;
        }
        auto pos = targetPosition;
        pos.x = std::clamp(pos.x, -160., 160.);
        pos.y = std::clamp(pos.y, -100., 100.);
        jngl::translate(t * -pos);
    }
}

void Game::stepMusic() {
    if (stepsLost == 0) {
        ScoreKeeper::handle().addDeath();
    }
    if (stepsLost >= 0) {
        volume += 0.11f * (1 - volume);
    } else {
        volume += 0.11f * (0.1 - volume);
    }
    jngl::loop("sfx/only_time.ogg")->setVolume(volume);
}
