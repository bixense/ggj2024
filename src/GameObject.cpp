#include "GameObject.hpp"

#include "constants.hpp"

#include <box2d/box2d.h>

GameObject::~GameObject() {
	if (body) {
		body->GetWorld()->DestroyBody(body);
	}
}

jngl::Vec2 GameObject::getPosition() const {
	return meterToPixel(body->GetPosition());
}

float GameObject::getRotation() const {
	return body->GetAngle();
}


void GameObject::setPosition(const jngl::Vec2 position) {
	const auto transform = body->GetTransform();
	body->SetTransform(pixelToMeter(position), transform.q.GetAngle());
}

void GameObject::createFixtureFromShape(const b2Shape& shape) {
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = FILTER_CATEGORY_SOLID_OBJECT;
	fixtureDef.filter.maskBits = 0xffff;
	fixture = body->CreateFixture(&fixtureDef);
	body->SetGravityScale(1);
}

void GameObject::onContact(GameObject* other) {
}

bool GameObject::checkOutOfScreen() {
	bool jumped = false;
	jngl::Vec2 pos = getPosition();
	float width = jngl::getScreenWidth() / 2.f;
	float height = jngl::getScreenHeight() / 2.f;

	float offset = 10.0;

	// ueber den Bildschirmrand
	if (pos.x < -width - offset) {
		setPosition({ width - offset, pos.y });
		jumped = true;
	}
	if (pos.x > width + offset) {
		setPosition({ -width + offset, pos.y });
		jumped = true;
	}
	if (pos.y > height + offset) {
		setPosition({ pos.x, -height +offset });
		jumped = true;
	}
	if (pos.y < -height - offset) {
		setPosition({ pos.x, height -offset});
		jumped = true;
	}
	return jumped;
}
