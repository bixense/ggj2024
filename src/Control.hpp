#pragma once

#include <jngl.hpp>

class Control {
public:
    virtual ~Control() = default;

    void step();
    bool hasTouched();
    jngl::Vec2 getMovement() const;
    jngl::Vec2 getTouchInput() const;
    bool fart() const;

private:
    bool fartingReleased = true;
    bool farting = false;
    int playerNr = 0;
    double lastTouchTime = -10;
    const std::vector<std::shared_ptr<jngl::Controller>> controllers =
        jngl::getConnectedControllers();
};
