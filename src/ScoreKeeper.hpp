#pragma once

#include <jngl.hpp>

class ScoreKeeper : public jngl::Singleton<ScoreKeeper> {
public:
    ScoreKeeper();

    void reset();
    void addDeath();
    int getTotalDeaths() const;
    int getDeathsFartyBird() const;
    int getDeathsAssEvaders() const;
    int getDeathsWhoopeeJump() const;
    int getDeathsAssteroids() const;

private:
    int deathsFartyBird = 0;
    int deathsAssEvaders = 0;
    int deathsWhoopeeJump = 0;
    int deathsAssteroids = 0;
};
