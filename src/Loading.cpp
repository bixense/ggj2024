#include "Loading.hpp"

#include "Menu.hpp"
#include "engine/Fade.hpp"

#include <datafiles.hpp>
#include <jngl.hpp>
#include <sstream>
#include <thread>
#include <vector>

Loading::Loading() {
    {
        std::istringstream filesString(SFX_FILES);
        std::string file;
        while (std::getline(filesString, file, ';')) {
            const std::string extension = ".ogg";
            if (file.substr(file.size() - extension.size()) == extension) {
                loadingFunctions.emplace_back([file]() { return jngl::load(file); });
            }
        }
    }
    {
        std::istringstream filesString(GFX_FILES);
        std::string file;
        while (std::getline(filesString, file, ';')) {
            const std::string extension = ".webp";
            if (file.substr(file.size() - extension.size()) == extension) {
                loadingFunctions.emplace_back([file]() { return jngl::load(file); });
            }
        }
    }
}

void Loading::step() {
    if (finished) {
        jngl::setWork<Menu>();
    }
}

void Loading::draw() const {
    // float percentage = 100;
    if (currentIndex != loadingFunctions.size()) {
        // percentage = float(currentIndex) / loadingFunctions.size() * 100;
        std::vector<jngl::Finally> loaders;
        for (size_t i = 0; i < std::thread::hardware_concurrency(); ++i) {
            loaders.emplace_back(loadingFunctions[currentIndex]());
            ++currentIndex;
            if (currentIndex == loadingFunctions.size()) {
                break;
            }
        }
    } else {
        finished = true;
    }
    jngl::setFontColor(0xffffff_rgb);
    jngl::setFontSize(10);
    jngl::print("loading ... [" + std::to_string(currentIndex) + "/" +
                    std::to_string(loadingFunctions.size()) + "]",
                -100, 0);
}
