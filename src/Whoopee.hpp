#pragma once

#include "Control.hpp"
#include "GameObject.hpp"
#include "engine/Animation.hpp"

#include <box2d/box2d.h>

class Whoopee : public GameObject {
public:
    explicit Whoopee(b2World&);
    ~Whoopee() override;
    Action step() override;
    void animate();

    std::unique_ptr<Control> control;

protected:
    jngl::Sprite sprite{ "fartibird/whoopee" };
    Animation animation;

private:
    void draw(jngl::Mat3) const override;

    constexpr static double RADIUS = 16.;
    int currentFartSound = 1;
	bool wasFartSoundPlaying = false;
};
