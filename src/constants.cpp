#include "constants.hpp"

#include <box2d/box2d.h>

constexpr float PIXEL_PER_METER = 100.0f;

jngl::Vec2 meterToPixel(const b2Vec2& v) {
 	return { v.x * PIXEL_PER_METER, v.y * PIXEL_PER_METER };
}

b2Vec2 pixelToMeter(const jngl::Vec2& v) {
	return b2Vec2(v.x / PIXEL_PER_METER, v.y / PIXEL_PER_METER);
}

float meterToPixel(double x) {
	return x * PIXEL_PER_METER;
}

float pixelToMeter(double x) {
	return x / PIXEL_PER_METER;
}
