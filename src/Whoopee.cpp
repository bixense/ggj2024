#include "Whoopee.hpp"

#include "constants.hpp"

#include <box2d/box2d.h>
#include <jngl.hpp>

Whoopee::Whoopee(b2World& world)
: control(std::make_unique<Control>()), animation("fartibird/fartyBird") {
    b2BodyDef bodyDef;
    bodyDef.position = pixelToMeter(jngl::Vec2(0, 0));
    bodyDef.type = b2_dynamicBody;
    body = world.CreateBody(&bodyDef);
    body->SetGravityScale(0);
    body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
    body->SetLinearDamping(10.f);
    body->SetTransform(body->GetPosition(), 3 * M_PI / 2.);

    b2CircleShape shape = b2CircleShape();
    shape.m_radius = pixelToMeter(RADIUS);
    createFixtureFromShape(shape);
}

Whoopee::~Whoopee() = default;

auto Whoopee::step() -> Action {
    animation.step();

    if (!animation.done()) {
        const int NUMBER_OF_SOUNDS = 4;
        if (!wasFartSoundPlaying ||
            !jngl::isPlaying("sfx/mouth" + std::to_string(currentFartSound) + ".ogg")) {
            auto old = currentFartSound;
            do {
                currentFartSound = (rand() % NUMBER_OF_SOUNDS) + 1;
            } while (old == currentFartSound);
            jngl::play("sfx/mouth" + std::to_string(currentFartSound) + ".ogg");
        }
        wasFartSoundPlaying = true;
    } else if (wasFartSoundPlaying) {
        wasFartSoundPlaying = false;
        jngl::stop("sfx/mouth" + std::to_string(currentFartSound) + ".ogg");
    }

    return Action::NONE;
}

void Whoopee::draw(jngl::Mat3 mv) const {
    auto a = getRotation();
    mv.translate(getPosition()).rotate(a - M_PI / 2);
    if (animation.done()) {
        sprite.draw(jngl::Mat3(mv).translate({ 0, 6 }));
    } else {
        animation.draw(jngl::Mat3(mv).translate({ 0, 9 }));
    }
    jngl::setColor(0, 255, 0, 150);
    // jngl::drawCircle(mv, RADIUS);
}

void Whoopee::animate() {
    animation.reset();
}
