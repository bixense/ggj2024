#include "Menu.hpp"

#include "ScoreKeeper.hpp"
#include "engine/Fade.hpp"
#include "fartybird/FartyBird.hpp"

#include <iostream>

Menu::Menu() {
    try {
        video = std::make_unique<jngl::Video>(
            "Slow_motion_whoopee_cushions.ogv"); // TODO: catch if video not supported
    } catch (std::runtime_error& e) {
        std::cout << e.what() << std::endl;
    }
    auto font = jngl::Font("fonts/BarcadeBrawlRegular-cc0.ttf", 50);
    start_text = new jngl::TextLine(font, "Fart to Start");

    texts = {
        { "Welcome", 50, { -68., -300. } },
        { "to", 130, { -68., -250. } },
        { "The Adventures of Farty McFartface", 260, { -68., -200. } },
        { "featuring:", 500, { 132, -70. } },
        { "Farty Bird", 600, { 132, 20. } },
        { "Ass Evaders", 700, { 132, 60. } },
        { "Whoopee Jump", 800, { 132, 100. } },
        { "Assteroids", 900, { 132, 140. } },
        { "Comming Soon:", 2000, { 132, -70. } },
        { "AssAssin's Creed", 2100, { 132, 20. } },
        { "Fart - A Link to the Arse", 2200, { 132, 60. } },
        { "Super Mario Fart", 2300, { 132, 100. } },
        { "Fartcry 3", 2400, { 132, 140. } },
        { "Breath of the Wild", 2500, { 132, 180. } },
        { "In loving Memory", 4050, { -68., -300. } },
        { "of all", 4130, { -68., -250. } },
        { "Whoopee Cushion Victims", 4260, { -68., -200. } },

        { "a game by", 7000, { 0, -400. } },
        { "Pablo Lubitz", 7100, { 0, -100. } },
        { "Kai Fuhrmann", 7200, { 0, 0. } },
        { "Kolja Lubitz", 7300, { 0, 100. } },
        { "Jan Niklas Hasse", 7400, { 0, 200. } },
    };
    jngl::loop("sfx/only_time.ogg")->setVolume(1);
}

void Menu::step() {
    wait -= 1;
    if (wait < 0) {
        alpha -= 1;
        if (alpha < 0) {
            alpha = 0;
        }
    }
    control->step();
    exit = control->fart();
    if (exit) {
        jngl::play("sfx/mouth2.ogg");
        ScoreKeeper::handle().reset();
        jngl::setWork<Fade>(std::make_shared<FartyBird>(1), 3);
    }
    for (const auto& t : texts) {
        if (t.appearAtStep == stepsPassed) {
            static jngl::Font font("fonts/BarcadeBrawlRegular-cc0.ttf", 20);
            auto label = container.addWidget<jngl::Label>(t.text, font, 0xffffff_rgb, t.pos);
            const float duration = 9. + static_cast<float>(t.text.size()) / 6.;
            label->addEffect<jngl::Executor>([label, duration](float t) {
                if (t < 1) {
                    label->setAlpha(jngl::easing::linear(t));
                }
                if (t > duration) {
                    label->setAlpha(1.f - jngl::easing::linear(t - duration));
                }
                if (t > duration + 1) {
                    return jngl::Effect::Action::REMOVE_WIDGET;
                }
                return jngl::Effect::Action::NONE;
            });
        }
    }
    ++stepsPassed;
    if (stepsPassed > 8500) {
        jngl::setWork<Menu>();
    }
    container.step();
}

void Menu::draw() const {
    // the video is 1920 x 1080
    jngl::pushMatrix();
    jngl::scale(jngl::getScreenHeight() / 1080.);
    if (video) {
        video->draw();
    } else {
        screenshot.draw();
    }
    jngl::setColor(0, 0, 0, alpha);
    jngl::drawRect({ -1920 / 2, -1080 / 2 }, { 1920, 1080 });
    jngl::setColor(255, 255, 255, 255);

    container.draw();

    jngl::setFontColor(255, 255, 255, exit ? 80 : 255);
    start_text->setCenter(-21, 400);
    if (exit || (stepsPassed > 510 && (stepsPassed / 30) % 2 == 0)) {
        start_text->draw();
    }
    jngl::popMatrix();
}
