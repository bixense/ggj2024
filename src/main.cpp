#include "Loading.hpp"
#include "constants.hpp"
#include "fartybird/FartyBird.hpp"
#include "assteroids/Assteroids.hpp"
#include "whoopeejump/WhoopeeJump.hpp"
#include "assevaders/Assevaders.hpp"
#include "cutscene/Cutscene1.hpp"
#include "cutscene/Cutscene2.hpp"
#include "cutscene/Cutscene3.hpp"

#include <ctime>
#include <jngl/main.hpp>
#include <jngl/message.hpp>
#include <jngl/input.hpp>
#include <jngl/job.hpp>
#include <jngl/init.hpp>

jngl::AppParameters jnglInit() {
	jngl::AppParameters params;
	std::srand(std::time(nullptr));
	params.displayName = programDisplayName;
	params.screenSize = { SCREEN_WIDTH, SCREEN_HEIGHT };
	params.pixelArt = true;

	params.start = []() -> std::shared_ptr<jngl::Work> {
		jngl::setBackgroundColor(0x000000_rgb);
		jngl::setMouseVisible(false);
		jngl::setFont("fonts/BarcadeBrawlRegular-cc0.ttf");
		auto args = jngl::getArgs();
		if (args.size() == 1) {

			if (args[0] == "--cutscene1") {
				return std::make_shared<Cutscene1>(nullptr);
			}
			if (args[0] == "--cutscene2") {
				return std::make_shared<Cutscene2>(nullptr);
			}
			if (args[0] == "--cutscene3") {
				return std::make_shared<Cutscene3>(std::make_shared<Assevaders>(0));
			}
			if (args[0] == "--assteroids") {
				return std::make_shared<Assteroids>(0);
			}
			if (args[0] == "--fartybird") {
				return std::make_shared<FartyBird>(0);
			}
			if (args[0] == "--whoopeejump") {
				return std::make_shared<WhoopeeJump>(0);
			}
			if (args[0] == "--assevaders") {
				return std::make_shared<Assevaders>(0);
			}
		}
		return std::make_shared<Loading>();
	};
	return params;
}
