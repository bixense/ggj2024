#pragma once

#include <jngl.hpp>

class b2Body;
class b2Fixture;
class b2Shape;

class GameObject {
public:
	virtual ~GameObject();

	enum class Action {
		NONE,
		REMOVE_ME,
	};

	virtual Action step() = 0;
	virtual void draw(jngl::Mat3 modelview) const = 0;

	jngl::Vec2 getPosition() const;
	float getRotation() const;
	void setPosition(jngl::Vec2 position);

	virtual void onContact(GameObject* other);

	bool checkOutOfScreen();

	b2Body* body = nullptr;

protected:
	void createFixtureFromShape(const b2Shape&);

	b2Fixture* fixture = nullptr;
};
