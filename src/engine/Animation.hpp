#pragma once

#include <jngl.hpp>
#include <string>

class Animation {
public:
    /// \a filename ohne Endung (.json und .webp wird angefügt)
    explicit Animation(const std::string& filename, bool loop = false);

    void step();
    void draw(jngl::Mat3 modelview) const;
    void reset();
    bool done() const;

private:
    jngl::Sprite sprite;
    int currentFrame = 99999;
    int duration;
	std::vector<std::vector<jngl::Vertex>> vertexes;
    std::vector<int> durations;
    bool loop;
};
