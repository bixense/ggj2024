#pragma once

#include <jngl/work.hpp>

class MoveFade : public jngl::Work {
public:
	explicit MoveFade(std::shared_ptr<jngl::Work>);
	void step() override;
	void draw() const override;

private:
	std::shared_ptr<Work> work_;
	std::shared_ptr<jngl::Work> oldWork_;
	float t = 1.f;
};
