#include "MoveFade.hpp"

#include "../constants.hpp"

#include <jngl.hpp>

MoveFade::MoveFade(std::shared_ptr<Work> work)
: work_(std::move(work)), oldWork_(jngl::getWork()) {
}

void MoveFade::step() {
	t *= 0.9f;
	if (t < 0.001f) { // Finished?
		jngl::setWork(work_);
        work_->step();
	}
}

void MoveFade::draw() const {
    if (!dynamic_cast<MoveFade*>(oldWork_.get())) {
        oldWork_->draw();
    }
    jngl::pushMatrix();
    jngl::translate(float(SCREEN_WIDTH) * t, 0);
    work_->draw();
    jngl::popMatrix();
}
