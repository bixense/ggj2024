#include "Animation.hpp"

#include <nlohmann/json.hpp>

Animation::Animation(const std::string& filename, bool loop) : sprite(filename), loop(loop) {
    auto j = nlohmann::json::parse(jngl::readAsset(filename + ".json"));
    std::map<int, nlohmann::basic_json<>> frames; 
    for (auto [key, val] : j["frames"].items()) {
        std::istringstream name(static_cast<std::string>(key));
        std::string tmp;
        name >> tmp;
        int number;
        name >> number;
        assert(name);
        auto result = frames.emplace(number, val);
        assert(result.second);
    }
    for (auto [key, val] : frames) {
        float x = val["frame"]["x"];
        float y = val["frame"]["y"];
        float w = val["frame"]["w"];
        float h = val["frame"]["h"];
        vertexes.push_back({
            { w / -2.f, h / -2.f, x / sprite.getWidth(), y / sprite.getHeight() },
            { w / -2.f, h / 2.f, x / sprite.getWidth(), (y + h) / sprite.getHeight() },
            { w / 2.f, h / -2.f, (x + w) / sprite.getWidth(), y / sprite.getHeight() },

            { w / -2.f, h / 2.f, x / sprite.getWidth(), y + h / sprite.getHeight() },
            { w / 2.f, h / -2.f, (x + w) / sprite.getWidth(), y / sprite.getHeight() },
            { w / 2.f, h / 2.f, (x + w) / sprite.getWidth(), (y + h) / sprite.getHeight() },
        });
        durations.push_back(static_cast<int>(std::lround(static_cast<double>(val["duration"]) /
                                                         (1000. / jngl::getStepsPerSecond()))));
    }
    if (loop) {
        reset();
    }
}

void Animation::step() {
    if (done()) {
        return;
    }
    --duration;
    if (duration <= 0) {
        ++currentFrame;
        if (done()) {
            if (loop) {
                reset();
            }
            return;
        }
        duration = durations[currentFrame];
    }
}

void Animation::draw(jngl::Mat3 modelview) const {
    sprite.drawMesh(
        modelview,
        vertexes[currentFrame >= vertexes.size() ? (vertexes.size() - 1) : currentFrame]);
}

void Animation::reset() {
    currentFrame = 0;
    duration = durations[currentFrame];
}

bool Animation::done() const {
    return currentFrame >= vertexes.size();
}
