#pragma once

#include <string>
#include <jngl/Vec2.hpp>

struct b2Vec2;

constexpr int SCREEN_WIDTH = 533;
constexpr int SCREEN_HEIGHT = 300;

enum {
	FILTER_CATEGORY_SOLID_OBJECT = 0x01,
	FILTER_CATEGORY_NON_SOLID_OBJECT = 0x02,
	FILTER_CATEGORY_PLAYER = 0x04,
	FILTER_CATEGORY_PLAYER1 = 0x08,
	FILTER_CATEGORY_PLAYER2 = 0x10,
	FILTER_CATEGORY_PLAYER3 = 0x20,
};

const std::string programDisplayName = "Farty McFartface";

jngl::Vec2 meterToPixel(const b2Vec2&);
b2Vec2 pixelToMeter(const jngl::Vec2&);
float meterToPixel(double);
float pixelToMeter(double);
