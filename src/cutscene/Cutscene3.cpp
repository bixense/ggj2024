#include "Cutscene3.hpp"

#include "../constants.hpp"
#include "../engine/Fade.hpp"
#include "../whoopeejump/WhoopeeJump.hpp"
#include "../fartybird/BirdWhoopee.hpp"

#include <iostream>

Cutscene3::Cutscene3(std::shared_ptr<Game> game) : Game(0, 0), game(std::move(game)) {
    whoopee =new BirdWhoopee(world, lost);
    gameObjects.emplace_back(whoopee);
    whoopee->setPosition({ -SCREEN_WIDTH / 2 + 50, 0 });
    whoopee->animate();
}

void Cutscene3::step() {
    whoopee->animate();
    whoopee->Whoopee::step();
    whoopee->body->SetTransform(pixelToMeter(whoopee->getPosition()), M_PI+travelspeed*100);
    whoopee->setPosition(
        { whoopee->getPosition().x + travelspeed * travelspeed, whoopee->getPosition().y });
    travelspeed += 0.01;
    ;
    speedlineTranslate += 25;
    if (speedlineTranslate > 261) {
        speedlineTranslate = 0;
    }
    if (!fadingOut && whoopee->getPosition().x > 40) {
        fadingOut = true;
        auto fade = std::make_shared<Fade>(game->getNext(), 6);
        fade->stepOut = true;
        jngl::setWork(fade);
    }
}

void Cutscene3::draw() const {
    auto mv = jngl::modelview().translate({ bg.getWidth() / 2. - SCREEN_WIDTH / 2., 0 });
    bg.draw(mv);
    bg3.draw(mv.translate({ -speedlineTranslate, 0 }));
    drawGameObjects(jngl::modelview());
}
