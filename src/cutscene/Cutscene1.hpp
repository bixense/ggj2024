#pragma once

#include "../Game.hpp"

class Whoopee;

class Cutscene1 : public Game {
public:
    explicit Cutscene1(std::shared_ptr<Game>);
    void step() override;
    void draw() const override;

private:

    jngl::Sprite bg{ "cutscene/background" };
    jngl::Sprite bg3{ "cutscene/part1_beforeforegroundv2" };

    bool stepped = false;
    bool farted = false;
    std::shared_ptr<Game> game;
    Whoopee* whoopee = nullptr;
};
