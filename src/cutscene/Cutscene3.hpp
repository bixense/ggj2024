#pragma once

#include "../Game.hpp"

class Whoopee;

class Cutscene3 : public Game {
public:
    Cutscene3(std::shared_ptr<Game>);
    void step() override;
    void draw() const override;
    
private:
    double travelspeed = 0;
    double speedlineTranslate = 0;
    jngl::Sprite bg{ "cutscene/background" };
    jngl::Sprite bg3{ "cutscene/speedlines_part3" };

    bool fadingOut = false;
    Whoopee* whoopee = nullptr;
    std::shared_ptr<Game> game;
};
