#pragma once

#include "../Game.hpp"
#include "../engine/Animation.hpp"

class Cutscene2 : public Game {
public:
    Cutscene2(std::shared_ptr<Game>);
    void step() override;
    void draw() const override;

private:

    jngl::Sprite bg{ "cutscene/background" };

    std::shared_ptr<Game> game;
    int stepsPassed = 0;
    Animation animation{"cutscene/censored_turnarround"};
};
