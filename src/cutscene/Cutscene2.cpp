#include "Cutscene2.hpp"

#include "../constants.hpp"
#include "../engine/Fade.hpp"
#include "../whoopeejump/WhoopeeJump.hpp"
#include "../fartybird/BirdWhoopee.hpp"
#include "Cutscene3.hpp"

#include <iostream>

Cutscene2::Cutscene2(std::shared_ptr<Game> game) : Game(0, 0), game(std::move(game)) {
    animation.reset();
}

void Cutscene2::step() {
    animation.step();
    if (animation.done()) {
        auto fade = std::make_shared<Fade>(std::make_shared<Cutscene3>(game), 8);
        fade->stepIn = true;
        jngl::setWork(fade);
    }
    if (stepsPassed == 100) {
        jngl::play("sfx/mouth2.ogg");
    }
    if (stepsPassed == 138) {
        jngl::play("sfx/mouth4.ogg");
    }
    ++stepsPassed;
}

void Cutscene2::draw() const {
    if (game) {
        game->draw();
    }
    animation.draw(jngl::modelview());
    auto mv = jngl::modelview().translate({ bg.getWidth() / 2. - SCREEN_WIDTH / 2., 0 });
    // bg.draw(mv);
    drawGameObjects(jngl::modelview());
}
