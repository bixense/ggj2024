#include "Cutscene1.hpp"

#include "../constants.hpp"
#include "../engine/Fade.hpp"
#include "../fartybird/BirdWhoopee.hpp"
#include "Cutscene2.hpp"

#include <iostream>

Cutscene1::Cutscene1(std::shared_ptr<Game> game) : Game(0, 0), game(std::move(game)), whoopee(new BirdWhoopee(world, lost)) {
    gameObjects.emplace_back(whoopee);

    whoopee->setPosition({ -9999, 74 });
    whoopee->body->SetTransform(whoopee->body->GetPosition(), M_PI);
}

void Cutscene1::step() {
    if (!stepped) {
        stepped = true;
        whoopee->setPosition({ -SCREEN_WIDTH / 2 - 50, 76 });
    }
    if (!farted && whoopee->getPosition().x > -250) {
        whoopee->animate();
        farted = true;
    }
    whoopee->Whoopee::step(); // keine Steuerung, daher nur die Basis-Methode
    // Game::step();
    whoopee->setPosition({ whoopee->getPosition().x + 1, whoopee->getPosition().y });
    if (whoopee->getPosition().x > -140) {
        whoopee->setPosition({ -300, -300 });
        jngl::setWork<Cutscene2>(std::move(game));
    };
}

void Cutscene1::draw() const {
    if (game && stepped) {
        game->draw();
    }
    auto mv = jngl::modelview().translate({ bg.getWidth() / 2. - SCREEN_WIDTH / 2., 0 });
    // bg.draw(mv);
    drawGameObjects(jngl::modelview());
    bg3.draw(mv);
}
