#pragma once

#include "Control.hpp"

#include <jngl.hpp>

class Highscore : public jngl::Work {
public:
    Highscore();
    void step() override;
    void draw() const override;

private:
    jngl::Container container;
    Control control;
};
