#pragma once

#include "ContactListener.hpp"
#include <box2d/box2d.h>
#include <jngl.hpp>

class GameObject;

class Game : public jngl::Work {
public:
    Game(float gravity, float musicVolume);
    void step() override;
    void add(std::function<std::shared_ptr<GameObject>()> factory);
    void remove(const std::shared_ptr<GameObject>& obj);

    virtual std::shared_ptr<Work> getNext() const;

    b2World world;
    bool lost = false;

protected:
    void drawGameObjects(const jngl::Mat3& modelview) const;
    void stepMusic();
    void deathAnimation(jngl::Vec2 targetPosition) const;

    std::vector<std::shared_ptr<GameObject>> gameObjects;
    int stepsLost = -1; // wenn verloren, zählt hoch
    float volume;

private:
    ContactListener contactListener;
    void addObjects();
    void removeObjects();
    std::vector<std::function<std::shared_ptr<GameObject>()>> needToAdd;
    std::vector<std::shared_ptr<GameObject>> needToRemove;
};
