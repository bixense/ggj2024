#pragma once

#include <jngl.hpp>

class Loading : public jngl::Work {
public:
	Loading();
	void step() override;
	void draw() const override;

private:
	mutable std::vector<std::function<jngl::Finally()>> loadingFunctions;
	mutable size_t currentIndex = 0;
	mutable bool finished = false;
};
