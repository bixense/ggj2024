#pragma once

#include "Control.hpp"

#include <jngl.hpp>
#include <vector>

class Menu : public jngl::Work {
public:
    Menu();
    void step() override;
    void draw() const override;

private:
    std::unique_ptr<jngl::Video> video;
    jngl::Sprite screenshot{ "screenshot" };
    jngl::TextLine *start_text;
    int alpha = 255;
    int wait = 320;
    std::unique_ptr<Control> control{ std::make_unique<Control>() };
    bool exit = false;
    int stepsPassed = 0;
    jngl::Container container;

    struct Text {
        std::string text;
        int appearAtStep;
        jngl::Vec2 pos;
    };
    std::vector<Text> texts;
};
