#include "Rock.hpp"

#include "../constants.hpp"
#include "../Control.hpp"
#include "AssWhoopee.hpp"
#include "../Fart.hpp"
#include "jngl/Vec2.hpp"

#include <box2d/box2d.h>
#include <jngl.hpp>

Rock::Rock(b2World& world, Game* game, jngl::Vec2 position, bool left_cheek, bool right_cheek, bool the_rock, bool sushi)
: control(std::make_unique<Control>()), left_cheek(left_cheek), right_cheek(right_cheek), the_rock(the_rock), sushi(sushi),
  game(game) {
    b2BodyDef bodyDef;
    int range = 0;

    if (position == jngl::Vec2{0, 0})
    {
        const int screen_min = 50;
        int screen_max = SCREEN_WIDTH;
        range = screen_max - screen_min + 1;
        int screen_x = std::rand() % range + screen_min;

        screen_max = SCREEN_HEIGHT;
        range = screen_max - screen_min + 1;
        int screen_y = std::rand() % range + screen_min;

        bodyDef.position = pixelToMeter(jngl::Vec2(screen_x, screen_y));
    }else {
        bodyDef.position = pixelToMeter(jngl::Vec2(position.x, position.y));
    }
    bodyDef.type = b2_dynamicBody;
    body = world.CreateBody(&bodyDef);
    body->SetGravityScale(0);
    body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
    body->SetLinearDamping(10.f);

    b2CircleShape shape = b2CircleShape();
    shape.m_radius = pixelToMeter(20);
    createFixtureFromShape(shape);

    int min = -MAX_SPEED;
    int max = MAX_SPEED;
    range = max - min + 1;
    int numx = std::rand() % range + min;
    int numy = std::rand() % range + min;
    force = jngl::Vec2(body->GetMass() * numx, body->GetMass() * numy);
}

Rock::~Rock() = default;

auto Rock::step() -> Action {
    body->ApplyForce(b2Vec2(force.x, force.y), body->GetWorldCenter(), true);
    checkOutOfScreen();
    return status;
}

void Rock::draw(jngl::Mat3 mvcenter) const {
    mvcenter.translate(getPosition());
    if (right_cheek) {
        auto mvright = jngl::Mat3(mvcenter).translate({ 14, 1 });
        if (left_cheek) {
            right.draw(mvright);
        } else {
            right.draw(mvcenter);
        }
    }

    if (left_cheek) {
        auto mvleft = jngl::Mat3(mvcenter).translate({ -13, 0 });
        if (right_cheek) {
            left.draw(mvleft);
        } else {
            left.draw(mvcenter);
        }
    }
    if(the_rock)
    {
        rock.draw(mvcenter);
    }
    if(sushi)
    {
        sushi_sprite.draw(mvcenter);
    }
}

void Rock::onContact(GameObject* other) {
	if (dynamic_cast<Fart*>(other))
	{
		if (doubblerock()) {
			left_cheek = false;
            int min = -MAX_SPEED;
            int max = MAX_SPEED;
            int range = max - min + 1;
            int numx = std::rand() % range + min;
            int numy = std::rand() % range + min;
            force = jngl::Vec2(body->GetMass() * numx, body->GetMass() * numy);

			game->add([game = game, pos = getPosition()]() { return std::make_shared<Rock>(game->world, game, pos, true, false); });
		}else {
			status = Action::REMOVE_ME;
		}
        dynamic_cast<Fart*>(other)->setDeleted();
	}
	if (dynamic_cast<AssWhoopee*>(other))
	{
		game->lost = true;
	}
}
