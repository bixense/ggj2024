#pragma once

#include "../Whoopee.hpp"
#include "../Game.hpp"

class AssWhoopee : public Whoopee {
public:
    explicit AssWhoopee(b2World&, Game* game);

    Action step() override;
    void onContact(GameObject* other) override;
    void fadeOut();

private:
    void draw(jngl::Mat3) const override;

    Game* game;
    constexpr static double fart_speed = .2;
    float size = 1.f;
};
