#include "Assteroids.hpp"

#include "../Highscore.hpp"
#include "../constants.hpp"
#include "../cutscene/Cutscene1.hpp"
#include "../engine/Fade.hpp"
#include "../engine/MoveFade.hpp"
#include "AssWhoopee.hpp"
#include "Rock.hpp"

Assteroids::Assteroids(float musicVolume)
: Game(0, musicVolume), whoopee(new AssWhoopee(world, this)) {
    gameObjects.emplace_back(whoopee);
    gameObjects.emplace_back(new Rock(world, this));
    gameObjects.emplace_back(new Rock(world, this));
    gameObjects.emplace_back(new Rock(world, this));
    gameObjects.emplace_back(new Rock(world, this, jngl::Vec2{ 0, 0 }, true, false));
    gameObjects.emplace_back(new Rock(world, this, jngl::Vec2{ 0, 0 }, false, true));
    gameObjects.emplace_back(new Rock(world, this, jngl::Vec2{ 0, 0 }, false, false, true));
    gameObjects.emplace_back(new Rock(world, this, jngl::Vec2{ 0, 0 }, false, false, false, true));
}

void Assteroids::step() {
    stepMusic();
    if (stepsLost >= 0) {
        ++stepsLost;
        if (stepsLost > 200) {
            jngl::setWork<Fade>(std::make_shared<Assteroids>(volume));
        }
        return;
    }
    if (lost) {
        stepsLost = 0;
    }

    int vader_count = 0;
    for (const auto& gameObject : gameObjects) {
        if (dynamic_cast<Rock*>(&*gameObject)) {
            vader_count++;
        }
    }
    if (winCounter < 0 &&
        (vader_count <= 0 || (jngl::keyDown(jngl::key::ControlL) && jngl::keyPressed('n')))) {
        winCounter = 0;
    }
    if (winCounter >= 0) {
        ++winCounter;
        whoopee->fadeOut();
    }
    if (winCounter > 120) {
        jngl::setWork<MoveFade>(std::make_shared<Cutscene1>(shared_from_this()));
    }

    Game::step();
}

void Assteroids::draw() const {
    jngl::pushMatrix();
    deathAnimation(whoopee->getPosition());
    auto mvcenter = jngl::modelview();
    bg.draw(mvcenter);
    drawGameObjects(mvcenter);
    jngl::popMatrix();
    if(whoopee->control->hasTouched()){
        auto mv = jngl::modelview().translate({SCREEN_WIDTH / 2. -50, SCREEN_HEIGHT / 2. -50}).rotate(M_PI);
        auto mvu = jngl::modelview().translate({-SCREEN_WIDTH / 2.+ 10 + SCREEN_WIDTH/4, SCREEN_HEIGHT / 2.-15 -SCREEN_HEIGHT / 4}).rotate(M_PI);
        auto mvl = jngl::modelview().translate({-SCREEN_WIDTH / 2. +100, SCREEN_HEIGHT / 2. -50}).rotate(M_PI/2);
        auto mvr = jngl::modelview().translate({-SCREEN_WIDTH / 2. +50+SCREEN_WIDTH/4, SCREEN_HEIGHT / 2. -50}).rotate(M_PI*1.5);
        jngl::setSpriteAlpha(180);
        touch.draw(mv);
        up.draw(mvu);
        left.draw(mvl);
        right.draw(mvr);
        jngl::setSpriteAlpha(255);
    }
}

std::shared_ptr<jngl::Work> Assteroids::getNext() const {
    return std::make_shared<Highscore>();
}
