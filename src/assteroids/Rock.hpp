#pragma once

#include "../Game.hpp"
#include "../GameObject.hpp"

#include <box2d/box2d.h>

class Control;


class Rock : public GameObject {
public:
    explicit Rock(b2World&, Game* game, jngl::Vec2 position = {0, 0}, bool left_cheek = true, bool right_cheek = true, bool the_rock = false, bool sushi = false);
    ~Rock() override;
    Action step() override;

protected:
    std::unique_ptr<Control> control;
    jngl::Sprite left{"assteroids/left_assteroid"};
    jngl::Sprite right{"assteroids/right_assteroid_bottom"};
    jngl::Sprite rock{"assteroids/therock"};
    jngl::Sprite sushi_sprite{"assteroids/sushi"};
    jngl::Vec2 force;
    bool left_cheek;
    bool right_cheek;
    bool the_rock;
    bool sushi;
    bool doubblerock() const {return left_cheek && right_cheek;};
    void onContact(GameObject* other) override;
private:
    void draw(jngl::Mat3) const override;

    constexpr static double MAX_SPEED = 10.;
    Game* game;
    Action status = Action::NONE;
};
