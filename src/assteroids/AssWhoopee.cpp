#include "AssWhoopee.hpp"
#include "../Fart.hpp"
#include "jngl/Vec2.hpp"
#include "jngl/debug.hpp"
#include "../constants.hpp"

AssWhoopee::AssWhoopee(b2World& world, Game* game) : Whoopee(world), game(game) {

    body->SetLinearDamping(0.7f);

	setPosition(jngl::Vec2{0,0});
}

auto AssWhoopee::step() -> Action {
	control->step();
	auto direction = control->getMovement();
	// float force = body->GetMass() * 200;
	// body->ApplyForce(b2Vec2(0, direction.x), body->GetWorldCenter(), true);
	body->SetAngularVelocity(direction.x * 4.0);

	if (direction.y>=0){
		direction.y =0;
	}
	float velocity = direction.y * -0.2; // Your desired velocity of the car.
	float angle = body->GetAngle(); // Body angle in radians.

	float velX = std::cos(angle) * velocity; // X-component.
	float velY = std::sin(angle) * velocity; // Y-component.

	// body->SetLinearVelocity(b2Vec2(velX, velY));
	body->ApplyForce(b2Vec2(velX, velY), body->GetWorldCenter(), true);


    checkOutOfScreen();

	if (control->fart())
	{
		animate();
		auto a = getRotation();
		jngl::Vec2 direction(std::cos(a), std::sin(a));
		direction *= fart_speed;
		auto velo = body->GetLinearVelocity();
		direction *= (pixelToMeter(velo.Length()) + 1.0);
		auto current_velo = body->GetLinearVelocity();
 		game->add([game= game, pos = getPosition(), dir = direction, current_velo= current_velo]() {
			auto fart = std::make_shared<Fart>(game->world, pos, dir);
			fart->body->SetLinearVelocity(current_velo);
			return fart;
			});
	}

    return Whoopee::step();
}

void AssWhoopee::onContact(GameObject* other) {
}

void AssWhoopee::draw(jngl::Mat3 mv) const {
    auto a = getRotation();
    mv.translate(getPosition()).rotate(a - M_PI / 2).scale(size);
    if (animation.done()) {
        sprite.draw(jngl::Mat3(mv).translate({ 0, 6 }));
    } else {
        animation.draw(jngl::Mat3(mv).translate({ 0, 9 }));
    }
    jngl::setColor(0, 255, 0, 150);
    // jngl::drawCircle(mv, RADIUS);
}

void AssWhoopee::fadeOut() {
	size *= 0.95f;
}
