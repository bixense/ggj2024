#pragma once

#include "../Game.hpp"
#include "../Whoopee.hpp"

class AssWhoopee;

class Assteroids : public Game, public std::enable_shared_from_this<Assteroids> {
public:
    explicit Assteroids(float musicVolume);
    void step() override;
    void draw() const override;

private:
    std::shared_ptr<jngl::Work> getNext() const override;

    jngl::Sprite bg{ "assteroids/assteroid_background" };
    jngl::Sprite touch{ "fartibird/whoopee" };
    jngl::Sprite up{ "fartibird/whoopee" };
    jngl::Sprite left{ "fartibird/whoopee" };
    jngl::Sprite right{ "fartibird/whoopee" };
    AssWhoopee* whoopee;
    int winCounter = -1;
};
