#include "ScoreKeeper.hpp"

#include "assevaders/Assevaders.hpp"
#include "assteroids/Assteroids.hpp"
#include "whoopeejump/WhoopeeJump.hpp"
#include "fartybird/FartyBird.hpp"

ScoreKeeper::ScoreKeeper() = default;

void ScoreKeeper::reset() {
    deathsFartyBird = deathsAssEvaders = deathsWhoopeeJump = deathsAssteroids = 0;
}

void ScoreKeeper::addDeath() {
    jngl::play("sfx/scream.ogg");
    if (dynamic_cast<Assevaders*>(jngl::getWork().get()) != nullptr) {
        deathsAssEvaders += 1;
    } else if (dynamic_cast<FartyBird*>(jngl::getWork().get()) != nullptr) {
        deathsFartyBird += 1;
    } else if (dynamic_cast<Assteroids*>(jngl::getWork().get()) != nullptr) {
        deathsAssteroids += 1;
    } else if (dynamic_cast<WhoopeeJump*>(jngl::getWork().get()) != nullptr) {
        deathsWhoopeeJump += 1;
    } else {
        assert(false);
        deathsFartyBird += 1;
    }
}

int ScoreKeeper::getTotalDeaths() const {
    return deathsAssEvaders + deathsFartyBird + deathsAssteroids + deathsWhoopeeJump;
}

int ScoreKeeper::getDeathsFartyBird() const {
    return deathsFartyBird;
}
int ScoreKeeper::getDeathsAssEvaders() const {
    return deathsAssEvaders;
}
int ScoreKeeper::getDeathsWhoopeeJump() const {
    return deathsWhoopeeJump;
}
int ScoreKeeper::getDeathsAssteroids() const {
    return deathsAssteroids;
}
