#pragma once

#include "Game.hpp"
#include "GameObject.hpp"
#include "engine/Animation.hpp"

#include <box2d/box2d.h>

class Control;

class Fart : public GameObject {
public:
    explicit Fart(b2World&, jngl::Vec2 position, jngl::Vec2 force);
    ~Fart() override;
    Action step() override;
    void setDeleted();

protected:
    Animation sprite{ "AssEvadors/toxictrade", true };
    jngl::Vec2 force;
    void createFixtureFromShape(const b2Shape& shape);

private:
    void draw(jngl::Mat3) const override;

    constexpr static double RADIUS = 5.;
    int lifetime = 140;
    Action status = Action::NONE;
};
