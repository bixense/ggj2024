#pragma once

#include "../Game.hpp"
#include "../Whoopee.hpp"

class Assevaders : public Game, public std::enable_shared_from_this<Assevaders> {
public:
    explicit Assevaders(float musicVolume);
    void step() override;
    void draw() const override;
    int getVadersCount();

private:
    std::shared_ptr<jngl::Work> getNext() const override;
    int winCounter = -1; //< zählt hoch, sobald gewonnen

    jngl::Sprite bg{"assteroids/assteroid_background"};
    jngl::Sprite touch{ "fartibird/whoopee" };
    jngl::Sprite left{ "fartibird/whoopee" };
    jngl::Sprite right{ "fartibird/whoopee" };
    Whoopee* whoopee;
};
