#pragma once

#include "../Whoopee.hpp"
#include "../Game.hpp"

class VaderWhoopee : public Whoopee {
public:
    explicit VaderWhoopee(b2World&, Game* game);

    Action step() override;
    void onContact(GameObject* other) override;

private:
    Game* game;
    constexpr static double fart_speed = -4.;
};
