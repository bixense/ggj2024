#include "Bunker.hpp"
#include "../Fart.hpp"
#include "../SpaceFart.hpp"
#include "Vader.hpp"
#include "jngl/Vec2.hpp"
#include "jngl/debug.hpp"
#include "../constants.hpp"

const jngl::Vec2 Bunker::BB(86., 34);

Bunker::Bunker(b2World& world, Game* game, jngl::Vec2 position, bool green)
: game(game), sprite{ green ? "AssEvadors/bunker_green" : "AssEvadors/bunker" } {
    b2BodyDef bodyDef;

    bodyDef.position = pixelToMeter(position);
    bodyDef.type = b2_dynamicBody;
    body = world.CreateBody(&bodyDef);
    body->SetGravityScale(0);
    body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
    body->SetLinearDamping(10.f);

    b2PolygonShape shape;
    shape.SetAsBox(pixelToMeter(BB.x / 2.), pixelToMeter(BB.y / 2.));
    createFixtureFromShape(shape);

    setPosition(position);
}

auto Bunker::step() -> Action {
    if (live <= 0) {
        return Action::REMOVE_ME;
    }
    return Action::NONE;
}

void Bunker::draw(jngl::Mat3 mv) const {
    mv.translate(getPosition());
    jngl::setSpriteAlpha(static_cast<uint8_t>(255.f * live));
    sprite.draw(jngl::Mat3(mv).translate({ 0, 6 }));
    jngl::setSpriteAlpha(255);

    // BB debug:
    // jngl::setColor(0, 255, 0, 150);
    // jngl::drawRect(mv.translate(BB / -2.), BB, 0xff0000_rgb);
}

void Bunker::onContact(GameObject* other) {
    if (dynamic_cast<Fart*>(other)) {
        dynamic_cast<Fart*>(other)->setDeleted();
        live -= 0.2f;
    }
    if (dynamic_cast<SpaceFart*>(other)) {
        dynamic_cast<SpaceFart*>(other)->setDeleted();
        live -= 0.1f;
    }
    // if (dynamic_cast<Vader*>(other)) {
    //     game->lost = true;
    // }
}
