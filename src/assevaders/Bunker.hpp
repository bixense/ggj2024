#pragma once

#include "../GameObject.hpp"
#include "../Game.hpp"

class Bunker : public GameObject {
public:
    explicit Bunker(b2World&, Game* game, jngl::Vec2 position, bool green);

    Action step() override;
    void onContact(GameObject* other) override;

private:
    void draw(jngl::Mat3) const override;

    Game* game;
    jngl::Sprite sprite;
    constexpr static double fart_speed = -4.;
    float live = 1.f;
    const static jngl::Vec2 BB;
};
