#include "Assevaders.hpp"

#include "../constants.hpp"
#include "../cutscene/Cutscene1.hpp"
#include "../engine/Fade.hpp"
#include "../engine/MoveFade.hpp"
#include "../whoopeejump/WhoopeeJump.hpp"
#include "jngl/work.hpp"
#include <math.h>
#include "Bunker.hpp"
#include "Vader.hpp"
#include "VaderWhoopee.hpp"

Assevaders::Assevaders(float musicVolume)
: Game(0, musicVolume), whoopee(new VaderWhoopee(world, this)) {
    gameObjects.emplace_back(whoopee);
    const double SPACING = 30;
    int row = 0;
    for (int i = 0; i < 14; i++) {
        gameObjects.emplace_back(new Vader(
            world, this,
            jngl::Vec2{ ((-SCREEN_WIDTH / 2.0) + 25) + (SPACING * i),
                        ((-SCREEN_HEIGHT / 2.0) + 10) + (50 * row) + (i % 2 == 0 ? 20 : 0) },
            row));
    }

    row = 1;
    for (int i = 0; i < 14; i++) {
        gameObjects.emplace_back(new Vader(
            world, this,
            jngl::Vec2{ ((-SCREEN_WIDTH / 2.0) + 25) + (SPACING * i),
                        ((-SCREEN_HEIGHT / 2.0) + 10) + (50 * row) + (i % 2 == 0 ? 20 : 0) },
            row));
    }

    row = 2;
    for (int i = 0; i < 14; i++) {
        gameObjects.emplace_back(new Vader(
            world, this,
            jngl::Vec2{ ((-SCREEN_WIDTH / 2.0) + 25) + (SPACING * i),
                        ((-SCREEN_HEIGHT / 2.0) + 10) + (50 * row) + (i % 2 == 0 ? 20 : 0) },
            row));
    }

    for (int i = 0; i < 4; i++) {
        gameObjects.emplace_back(new Bunker(
            world, this, jngl::Vec2{ ((-SCREEN_WIDTH / 2.0) + 70) + (130 * i), 60 }, i % 2 == 0));
    }
}

void Assevaders::step() {
    stepMusic();
    if (stepsLost >= 0) {
        ++stepsLost;
        if (stepsLost > 200) {
            jngl::setWork<Fade>(std::make_shared<Assevaders>(volume));
        }
        return;
    }
    if (lost) {
        stepsLost = 0;
    }

    if (winCounter < 0 &&
        (getVadersCount() <= 0 || (jngl::keyDown(jngl::key::ControlL) && jngl::keyPressed('n')))) {
        winCounter = 0;
    }
    if (winCounter >= 0) {
        ++winCounter;
        whoopee->setPosition(whoopee->getPosition() + jngl::Vec2(0, 0.8));
    }
    if (winCounter > 60) {
        jngl::setWork<MoveFade>(std::make_shared<Cutscene1>(shared_from_this()));
    }

    Game::step();
}

void Assevaders::draw() const {
    jngl::pushMatrix();
    deathAnimation(whoopee->getPosition());
    auto mvcenter = jngl::modelview();
    bg.draw(mvcenter);
    drawGameObjects(mvcenter);
    if(whoopee->control->hasTouched()){
        auto mv = jngl::modelview().translate({SCREEN_WIDTH / 2. -50, SCREEN_HEIGHT / 2. -50}).rotate(M_PI);
        auto mvl = jngl::modelview().translate({-SCREEN_WIDTH / 2. +100, SCREEN_HEIGHT / 2. -50}).rotate(M_PI/2);
        auto mvr = jngl::modelview().translate({-SCREEN_WIDTH / 2. +50+SCREEN_WIDTH/4, SCREEN_HEIGHT / 2. -50}).rotate(M_PI*1.5);
        jngl::setSpriteAlpha(180);
        touch.draw(mv);
        left.draw(mvl);
        right.draw(mvr);
        jngl::setSpriteAlpha(255);
    }
    jngl::popMatrix();
}

int Assevaders::getVadersCount() {
    int vader_count = 0;
    for (const auto& gameObject : gameObjects) {
        if (dynamic_cast<Vader*>(&*gameObject)) {
            vader_count++;
        }
    }
    return vader_count;
}

std::shared_ptr<jngl::Work> Assevaders::getNext() const {
    return std::make_shared<WhoopeeJump>(volume);
}
