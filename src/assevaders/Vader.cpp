#include "Vader.hpp"

#include "../constants.hpp"
#include "../Control.hpp"
#include "Assevaders.hpp"
#include "VaderWhoopee.hpp"
#include "../Fart.hpp"
#include "../SpaceFart.hpp"
#include "jngl/Vec2.hpp"

#include <box2d/box2d.h>
#include <jngl.hpp>

Vader::Vader(b2World& world, Assevaders* game, jngl::Vec2 position, int row)
: control(std::make_unique<Control>()),
  death{ row % 2 == 1 ? "AssEvadors/dyingass" : "AssEvadors/pinkassdying" }, game(game), row(row) {
    b2BodyDef bodyDef;

    bodyDef.position = pixelToMeter(position);
    bodyDef.type = b2_dynamicBody;
    body = world.CreateBody(&bodyDef);
    body->SetGravityScale(0);
    body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
    body->SetLinearDamping(10.f);

    b2CircleShape shape = b2CircleShape();
    shape.m_radius = pixelToMeter(15);
    createFixtureFromShape(shape);
}

Vader::~Vader() = default;

auto Vader::step() -> Action {
    death.step();
    int vaders_left = game->getVadersCount();
    int min = 0;
    int max = 700;
    int range = max - min + 1;
    int num = std::rand() % range + min;
    if (num == 50 && (row != 0 || vaders_left <= 13)) {
        game->add([game = game, pos = getPosition()]() {
            return std::make_shared<SpaceFart>(game->world, pos + jngl::Vec2{ 0, 30 },
                                               jngl::Vec2{ 0, .1 });
        });
    }

    if (vaders_left < 20) {
        SPEED = 0.2;
        SIDE = 225;
    }
    if (vaders_left < 15) {
        SPEED = 0.3;
        SIDE = 165;
    }
    if (vaders_left < 10) {
        SPEED = 0.4;
        SIDE = 135;
    }
    if (vaders_left < 5) {
        SPEED = 0.5;
        SIDE = 90;
    }

    counter--;
    switch (mode) {
    case Mode::LEFT:
        body->ApplyForce(b2Vec2(SPEED, 0), body->GetWorldCenter(), true);
        if (counter <= 0) {
            mode = Mode::DOWN;
            counter = DOWN;
        }
        break;
    case Mode::RIGHT:
        body->ApplyForce(b2Vec2(-SPEED, 0), body->GetWorldCenter(), true);
        if (counter <= 0) {
            mode = Mode::DOWN;
            counter = DOWN;
        }
        break;
    case Mode::DOWN:
        body->ApplyForce(b2Vec2(0, SPEED), body->GetWorldCenter(), true);
        if (counter <= 0) {
            if (last_side == Mode::LEFT) {
                mode = Mode::RIGHT;
                last_side = Mode::RIGHT;
            } else {
                mode = Mode::LEFT;
                last_side = Mode::LEFT;
            }
            counter = SIDE;
        }
        break;
    }
    if (getPosition().y >= SCREEN_HEIGHT / 2) {
        game->lost = true;
    }

    if (!death.done() && fixture) {
        // hier noch nen Sound abspielen?
        body->DestroyFixture(fixture);
        fixture = nullptr;
    }
    if (death.done() && !fixture) {
        return Action::REMOVE_ME;
    }
    return Action::NONE;
}

void Vader::draw(jngl::Mat3 mv) const {
    mv.translate(getPosition());
    if (death.done() && fixture) {
        if (row % 2) {
            ass1.draw(mv);
        } else {
            ass2.draw(mv);
        }
    }
    if (!death.done() && !fixture) {
        death.draw(mv);
    }
    // jngl::setColor(0, 255, 0, 150);
    // jngl::drawCircle(mv, 15);
}

void Vader::onContact(GameObject* other) {
    if (dynamic_cast<Fart*>(other)) {
        death.reset();
        dynamic_cast<Fart*>(other)->setDeleted();
    }

    if (dynamic_cast<VaderWhoopee*>(other)) {
        game->lost = true;
    }
}
