#pragma once

#include "../Game.hpp"
#include "../GameObject.hpp"
#include "../engine/Animation.hpp"

#include <box2d/box2d.h>

class Assevaders;
class Control;

class Vader : public GameObject {
public:
    explicit Vader(b2World&, Assevaders* game, jngl::Vec2 position, int row);
    ~Vader() override;
    Action step() override;

protected:
    std::unique_ptr<Control> control;
    jngl::Sprite ass1{ "AssEvadors/ass01" };
    jngl::Sprite ass2{ "AssEvadors/ass02" };
    Animation death;
    void onContact(GameObject* other) override;

private:
    void draw(jngl::Mat3) const override;

    enum class Mode {
        LEFT,
        DOWN,
        RIGHT,
    };
    Assevaders* game;
    Mode mode = Mode::LEFT;
    int SIDE = 310; // wie weit die nach rechts und links gehen
    constexpr static int DOWN = 40;
    float SPEED = 0.1;
    int counter = SIDE;
    Mode last_side = Mode::LEFT;
    int row;
};
