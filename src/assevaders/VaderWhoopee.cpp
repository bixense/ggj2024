#include "VaderWhoopee.hpp"
#include "../Fart.hpp"
#include "../SpaceFart.hpp"
#include "../constants.hpp"

VaderWhoopee::VaderWhoopee(b2World& world, Game* game) : Whoopee(world), game(game) {
	setPosition(jngl::Vec2{0, SCREEN_HEIGHT/2 - 5});
    body->SetTransform(body->GetPosition(), 3 * M_PI / 2.);
}

auto VaderWhoopee::step() -> Action {
	control->step();
	auto direction = control->getMovement();
	direction *= 2.5;

	body->ApplyForce(b2Vec2(direction.x, 0), body->GetWorldCenter(), true);

	const auto pos = getPosition();
	if (pos.x < -SCREEN_WIDTH / 2.) {
		setPosition({ -SCREEN_WIDTH / 2., pos.y });
	}
	if (pos.x > SCREEN_WIDTH / 2.) {
		setPosition({ SCREEN_WIDTH / 2., pos.y });
	}

	if (control->fart())
	{
		if (animation.done()) {
			animate();
		}
		auto current_velo = body->GetLinearVelocity();
		game->add([game= game, pos = getPosition(), current_velo= current_velo]() {
			auto fart = std::make_shared<Fart>(game->world, pos + jngl::Vec2{0, -25}, jngl::Vec2{0, -.1});
			fart->body->SetLinearVelocity(current_velo);
			return fart;
			});
	}

    return Whoopee::step();
}

void VaderWhoopee::onContact(GameObject* other) {
	if (dynamic_cast<SpaceFart*>(other)) {
		game->lost = true;
	}
}
