#include "FartyBird.hpp"

#include "../assevaders/Assevaders.hpp"
#include "../constants.hpp"
#include "../cutscene/Cutscene1.hpp"
#include "../engine/Fade.hpp"
#include "../engine/MoveFade.hpp"
#include "BirdWhoopee.hpp"
#include "Pipe.hpp"

FartyBird::FartyBird(float musicVolume)
: Game(20, musicVolume), whoopee(new BirdWhoopee(world, lost)) {
    gameObjects.emplace_back(whoopee);
}

void FartyBird::step() {
    stepMusic();
    if (stepsLost >= 0) {
        ++stepsLost;
        if (stepsLost > 200) {
            jngl::setWork<Fade>(std::make_shared<FartyBird>(volume));
        }
        return;
    }
    if (lost) {
        stepsLost = 0;
    }
    Game::step();

    --countdown;
    const int WON_AT = 2200;
    if (countdown <= 0 && stepsPassed < WON_AT) {
        countdown = std::max(90 - difficulty / 10, 30);
        offset += static_cast<int>(random() % (41 + difficulty)) - (20 + difficulty / 2.);
        if (offset < -50) {
            offset = -30;
        }
        if (offset > 50) {
            offset = 30;
        }
        {
            auto pipe = new Pipe(world, true, random());
            pipe->setPosition(jngl::Vec2(330, -150 - distance + offset));
            gameObjects.emplace_back(pipe);
        }
        {
            auto pipe =
                new Pipe(world, false, std::lround(static_cast<double>(random() % 60) / 100.));
            pipe->setPosition(jngl::Vec2(330, 150 + distance + offset));
            gameObjects.emplace_back(pipe);
        }
        if (distance > 0) {
            distance -= 2;
        }
        ++difficulty;
    }
    if (stepsPassed > WON_AT + 100) {
        whoopee->setPosition(whoopee->getPosition() - jngl::Vec2(0.8, 0));
    }
    if (stepsPassed > WON_AT + 330 ||
        (jngl::keyDown(jngl::key::ControlL) && jngl::keyPressed('n'))) {
        jngl::setWork<MoveFade>(std::make_shared<Cutscene1>(shared_from_this()));
    }

    ++stepsPassed;
}

void FartyBird::draw() const {
    constexpr bool DEBUG = false;
    if constexpr (DEBUG) {
        jngl::pushMatrix();
        jngl::scale(0.4);
    }
    jngl::pushMatrix();
    if (stepsLost >= 0) {
        jngl::scale(1 + (stepsLost / 100.));
        jngl::rotate(stepsLost / 10.);
        float t = stepsLost / 200.;
        if (t > 1) {
            t = 1;
        }
        auto pos = whoopee->getPosition();
        pos.y = std::clamp(pos.y, -100., 100.);
        jngl::translate(t * -pos);
    }
    {
        double bgScroll = -((stepsPassed / 2) % std::lround(bg.getWidth()));
        auto mv =
            jngl::modelview().translate({ bgScroll + bg.getWidth() / 2. - SCREEN_WIDTH / 2., 0 });
        bg.draw(mv);
        bg.draw(jngl::Mat3(mv).translate({ bg.getWidth(), 0 }));
    }
    {
        double bgScroll = -(std::lround(stepsPassed * 1.3) % std::lround(bg2.getWidth()));
        double yOffset = (std::sin(stepsPassed / 700.) + 1.) * 30;
        assert(yOffset >= 0);
        auto mv =
            jngl::modelview().translate({ bgScroll + bg2.getWidth() / 2. - SCREEN_WIDTH / 2.,
                                          SCREEN_HEIGHT / 2. - bg2.getHeight() / 2. + yOffset });
        bg2.draw(mv);
        bg2.draw(jngl::Mat3(mv).translate({ bg2.getWidth(), 0 }));
    }
    drawGameObjects(jngl::modelview());
    {
        double bgScroll = -(std::lround(stepsPassed * 2.2) % std::lround(bg3.getWidth()));
        double yOffset = (std::cos(stepsPassed / 800.) + 1.) * 10;
        auto mv =
            jngl::modelview().translate({ bgScroll + bg3.getWidth() / 2. - SCREEN_WIDTH / 2.,
                                          SCREEN_HEIGHT / 2. - bg3.getHeight() / 2. + yOffset });
        jngl::setSpriteAlpha(180);
        bg3.draw(mv);
        bg3.draw(jngl::Mat3(mv).translate({ bg3.getWidth(), 0 }));
        jngl::setSpriteAlpha(255);
    }
    jngl::popMatrix();
    if constexpr (DEBUG) {
        jngl::setColor(0xff0000_rgb, 255);
        jngl::drawLine(jngl::Vec2(-SCREEN_WIDTH / 2., -SCREEN_HEIGHT / 2.),
                       jngl::Vec2(-SCREEN_WIDTH / 2., SCREEN_HEIGHT / 2.));
        jngl::drawLine(jngl::Vec2(SCREEN_WIDTH / 2., -SCREEN_HEIGHT / 2.),
                       jngl::Vec2(SCREEN_WIDTH / 2., SCREEN_HEIGHT / 2.));
        jngl::drawLine(jngl::Vec2(-SCREEN_WIDTH / 2., SCREEN_HEIGHT / 2.),
                       jngl::Vec2(SCREEN_WIDTH / 2., SCREEN_HEIGHT / 2.));
        jngl::drawLine(jngl::Vec2(-SCREEN_WIDTH / 2., -SCREEN_HEIGHT / 2.),
                       jngl::Vec2(SCREEN_WIDTH / 2., -SCREEN_HEIGHT / 2.));
        jngl::popMatrix();
    }
    if(whoopee->control->hasTouched()){
        auto mv = jngl::modelview().translate({SCREEN_WIDTH / 2. -50, SCREEN_HEIGHT / 2. -50});
        jngl::setSpriteAlpha(180);
        touch.draw(mv);
        jngl::setSpriteAlpha(255);
    }
}

std::shared_ptr<jngl::Work> FartyBird::getNext() const {
    return std::make_shared<Assevaders>(volume);
}
