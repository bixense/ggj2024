#pragma once

#include "../Game.hpp"
#include "../Whoopee.hpp"


#include <random>

class FartyBird : public Game, public std::enable_shared_from_this<FartyBird> {
public:
    explicit FartyBird(float musicVolume);
    void step() override;
    void draw() const override;
    std::shared_ptr<Work> getNext() const override;

private:
    int countdown = 0;
    int distance = 50;
    int offset = 0;
    int difficulty = 1; //< wird größer
    std::random_device random;
    jngl::Sprite bg{ "fartibird/clouds_background_layer" };
    jngl::Sprite bg2{ "fartibird/clouds_beforebackground_layer" };
    jngl::Sprite bg3{ "fartibird/clouds_vignette_layer" };
    jngl::Sprite touch{ "fartibird/whoopee" };
    int stepsPassed = 0;
    bool lost = false;
    Whoopee* whoopee = nullptr;
};
