#include "BirdWhoopee.hpp"

#include "../constants.hpp"

BirdWhoopee::BirdWhoopee(b2World& world, bool& lost) : Whoopee(world), lost(lost) {
    setPosition({ -120, -200 });
    body->SetTransform(body->GetPosition(), M_PI / 2.);
}

auto BirdWhoopee::step() -> Action {
	control->step();
    if (control->fart()) {
        animate();
        float force = body->GetMass() * 500;
        body->ApplyForce(b2Vec2(0, -force), body->GetWorldCenter(), true);
    }
    if (getPosition().y > SCREEN_HEIGHT / 2. ||
        getPosition().y <
            -SCREEN_HEIGHT /* nicht durch 2 damit man etwas nach oben raus fliegen kann*/) {
        lost = true;
    }
    return Whoopee::step();
}

void BirdWhoopee::onContact(GameObject*) {
    lost = true;
}
