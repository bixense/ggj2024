#include "Pipe.hpp"

#include "../constants.hpp"
#include "../engine/Animation.hpp"

const jngl::Vec2 Pipe::BB(50., 206.);

Pipe::Pipe(b2World& world, bool top, unsigned int variant)
: sprite(top ? (variant % 3 == 0
                    ? "fartibird/rohr_oben"
                    : (variant % 3 == 1 ? "fartibird/rohr_oben02" : "fartibird/rohr_oben03"))
             : (variant % 2 == 0 ? "fartibird/rohr_unten" : "fartibird/rohr_unten_livingplant")) {
    if (!top && variant % 2 != 0) {
        livingPlant = true;
    }
    b2BodyDef bodyDef;
    bodyDef.position = pixelToMeter(jngl::Vec2(-100, 0));
    bodyDef.type = b2_staticBody;
    body = world.CreateBody(&bodyDef);
    body->SetGravityScale(0);
    body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
    body->SetLinearDamping(10.f);

    b2PolygonShape shape;
    shape.SetAsBox(pixelToMeter(BB.x / 2.), pixelToMeter(BB.y / 2.));
    createFixtureFromShape(shape);
}

auto Pipe::step() -> Action {
    setPosition(getPosition() - jngl::Vec2(2, 0));
    if (livingPlant && getPosition().x < 50) {
        livingPlant = false;
        animation = std::make_unique<Animation>("fartibird/dying_plant");
        animation->reset();
    }
    if (animation) {
        animation->step();
        if (animation->done()) {
            sprite = jngl::Sprite("fartibird/rohr_unten02");
        }
    }
    if (getPosition().x < -jngl::getScreenWidth() / 2. - sprite.getWidth()) {
        return Action::REMOVE_ME;
    }
    return Action::NONE;
}

void Pipe::draw(jngl::Mat3 modelview) const {
    auto mv = jngl::Mat3(modelview).translate(getPosition());
    if (livingPlant) {
        mv.translate({ 4, -22 });
    }
    if (animation && !animation->done()) {
        animation->draw(mv.translate({ 25, -22.5 }));
    } else {
        sprite.draw(mv);
    }
    // jngl::drawRect(modelview.translate(getPosition() - BB / 2.), BB,
    //                jngl::Rgba(0xff0000_rgb, jngl::Alpha::u8(200)));
}
