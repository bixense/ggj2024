#pragma once

#include "../Whoopee.hpp"

class BirdWhoopee : public Whoopee {
public:
    explicit BirdWhoopee(b2World&, bool& lost);

    Action step() override;

    void onContact(GameObject*) override;

private:
    bool& lost;
};
