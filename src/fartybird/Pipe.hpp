#pragma once

#include "../GameObject.hpp"

#include <box2d/box2d.h>

class Animation;

class Pipe : public GameObject {
public:
    explicit Pipe(b2World&, bool top, unsigned int variant /* andere Grafik? */);
    Action step() override;

private:
    void draw(jngl::Mat3) const override;

    bool livingPlant = false;
    jngl::Sprite sprite;
    std::unique_ptr<Animation> animation;
    const static jngl::Vec2 BB;
};
