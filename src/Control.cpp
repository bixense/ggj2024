#include "Control.hpp"
#include "constants.hpp"

template <class Key> jngl::Vec2 findMovement(Key up, Key down, Key left, Key right) {
    jngl::Vec2 direction;
    if (jngl::keyDown(left)) {
        direction -= jngl::Vec2(1, 0);
    }
    if (jngl::keyDown(right)) {
        direction += jngl::Vec2(1, 0);
    }
    if (jngl::keyDown(up)) {
        direction -= jngl::Vec2(0, 1);
    }
    if (jngl::keyDown(down)) {
        direction += jngl::Vec2(0, 1);
    }
    return direction;
}

jngl::Vec2 Control::getMovement() const {

    jngl::Vec2 sum;
    if (controllers.size() > 0) {
        auto controller = controllers[0];

        const jngl::Vec2 dpad(-controller->state(jngl::controller::DpadLeft) +
                                  controller->state(jngl::controller::DpadRight),
                              -controller->state(jngl::controller::DpadUp) +
                                  controller->state(jngl::controller::DpadDown));
        const jngl::Vec2 sticks(controller->state(jngl::controller::LeftStickX),
                                -controller->state(jngl::controller::LeftStickY));
        sum += dpad + sticks;
    }

    sum += findMovement(jngl::key::Up, jngl::key::Down, jngl::key::Left, jngl::key::Right);
    sum += findMovement('w', 's', 'a', 'd');
    sum += getTouchInput();
    if (boost::qvm::mag_sqr(sum) > 1) {
        return boost::qvm::normalized(sum);
    }
    return sum;
}
jngl::Vec2 Control::getTouchInput() const {
    auto touches = jngl::getTouchPositions();
    jngl::Vec2 result{ 0, 0 };
    for (auto touch : touches) {
        if (touch.x < 0) {
            if (touch.y > SCREEN_HEIGHT / 4) {
                if (touch.x > (-SCREEN_WIDTH / 4)) {
                    result.x = 1;
                } else {
                    result.x = -1;
                }
            } else {
                result.y = -1;
            }
        }
    }
    return result;
}

bool Control::hasTouched(){
        return (lastTouchTime > jngl::getTime()- 3);
}

void Control::step() {
    auto touches = jngl::getTouchPositions();
    auto rightSideDown = [&]() {
        for (auto touch : touches) {
            lastTouchTime = jngl::getTime();
            if (touch.x > 0) {
                return true;
            }
        }
        return false;
    };
    auto smell = [&]() {
        if (jngl::keyPressed(jngl::key::Space)) {
            return fartingReleased;
        }
        for (const auto& controller : controllers) {
            if (controller->pressed(jngl::controller::A)) {
                return true;
            }
        }
        if (rightSideDown()) {
            return fartingReleased;
        }
        return false;
    };
    farting = smell();

    fartingReleased = !jngl::keyDown(jngl::key::Space) && !rightSideDown();
}

bool Control::fart() const {
    return farting;
}
