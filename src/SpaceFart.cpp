#include "SpaceFart.hpp"

#include "constants.hpp"
#include "Control.hpp"
#include "assteroids/Rock.hpp"
#include "jngl/Vec2.hpp"
#include "jngl/shapes.hpp"

#include <box2d/box2d.h>
#include <jngl.hpp>

SpaceFart::SpaceFart(b2World& world, jngl::Vec2 position, jngl::Vec2 force) : force(force) {
    b2BodyDef bodyDef;
    bodyDef.position = pixelToMeter(position);
    bodyDef.type = b2_dynamicBody;
    body = world.CreateBody(&bodyDef);
    body->SetGravityScale(0);
    body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
    body->SetLinearDamping(10.f);

    b2CircleShape shape = b2CircleShape();
    shape.m_radius = pixelToMeter(RADIUS);
    createFixtureFromShape(shape);
}

SpaceFart::~SpaceFart() = default;

auto SpaceFart::step() -> Action {
    lifetime--;
    if (lifetime <= 0) {
        return Action::REMOVE_ME;
    }
    body->ApplyForce(b2Vec2(force.x, force.y), body->GetWorldCenter(), true);
    if (checkOutOfScreen()) {
        return Action::REMOVE_ME;
    }
    return status;
}

void SpaceFart::draw(jngl::Mat3 mvcenter) const {
    mvcenter.translate(getPosition());
    sprite.draw(mvcenter);
}

void SpaceFart::createFixtureFromShape(const b2Shape& shape) {
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &shape;
    fixtureDef.density = 1.0f;
    fixtureDef.isSensor = true;
    fixtureDef.friction = 0.7f;
    fixtureDef.restitution = 0.1f;
    fixtureDef.filter.categoryBits = FILTER_CATEGORY_SOLID_OBJECT;
    fixtureDef.filter.maskBits = 0xffff;
    body->CreateFixture(&fixtureDef);
    body->SetGravityScale(1);
}

void SpaceFart::setDeleted()
{
    status = Action::REMOVE_ME;
}
